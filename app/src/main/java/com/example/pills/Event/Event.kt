package com.example.pills.Event

import android.os.Parcel
import android.os.Parcelable


class Event() : Parcelable {
    var id:Int = 0
    var date:Int=0
    var time:String= ""
    var event:String=""
    var isp:Int=0


    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        date=parcel.readInt()
        time=parcel.readString()
        event=parcel.readString()
        isp=parcel.readInt()
    }

    override   fun toString(): String {
        return "Contact(id=$id,date=$date,time=$time,event=$event,isp=$isp"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(date)
        parcel.writeString(time)
        parcel.writeString(event)
        parcel.writeInt(isp)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Event> {
        override fun createFromParcel(parcel: Parcel): Event {
            return Event(parcel)
        }

        override fun newArray(size: Int): Array<Event?> {
            return arrayOfNulls(size)
        }
    }
}