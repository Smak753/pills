package com.example.pills.Event

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.pills.Notification.DataNotif
import com.example.pills.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.event_item.view.*
import kotlin.properties.Delegates


class EventPageAdapter(): RecyclerView.Adapter<EventPageAdapter.EventViewHolder>(), Parcelable {
    var list1:ArrayList<Event>?=null
    var sortedList:List<Event>?=null
    var context: Context?=null
    var activity:Activity?=null
    var dt:Int?=null
    var sctn:String=""
    constructor(context: Context,date:Int,section:String,activity: Activity):this(){
        this.list1= DataDate.getInstance(context).getDayEvent(date,section)
        this.activity=activity
        this.dt=date
        this.sctn=section
        this.sortedList= list1?.sortedWith(compareBy ({it.date},{it.time}))
        this.context=context
    }
    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        var day=sortedList?.get(position)?.date.toString()
        var date=day[0]+""+day[1]+day[2]+day[3]+"-"+day[4]+day[5]+"-"+day[6]+day[7]
        holder.date.text = date
        holder.time.text = sortedList?.get(position)?.time
        holder.event.text = sortedList?.get(position)?.event
        when(sortedList!!.get(position)!!.isp){
            0-> holder.isp.text = "Не исполнено"
                    1->holder.isp.text = "Исполнено"
                2->holder.isp.text = "Отложено"
                3->holder.isp.text = "Отменено"
        }
        holder.btn_delete.setOnClickListener({
            val alert = AlertDialog.Builder(activity)
            alert.setTitle("Удаление")
                    alert.setMessage(holder.date.text.toString()+" "+holder.time.text.toString()+" "+ holder.event.text.toString())
                            .setCancelable(false)
                            .setPositiveButton("Удалить",{dialog, i ->
                                DataDate.getInstance(context!!).deleteEvent(sortedList!!.get(position)!!.id)
                                DataNotif.getInstance(context!!).deleteNotif(sortedList!!.get(position)!!.id)
                                dialog.dismiss()
                                dataChanged(DataDate.getInstance(context!!).getDayEvent(dt!!,sctn!!))
            })
            .setNegativeButton( "Отмена",{dialog, i ->
            dialog.cancel()
            })
            alert.create()
            alert.show()
        })
        holder.btn_edit.setOnClickListener({
            val alert = AlertDialog.Builder(activity)
            alert.setTitle("Изменение")

            val li = LayoutInflater.from(context)
            val alertView = li.inflate(R.layout.edit_alert, null)
            alert.setMessage(holder.date.text.toString()+" "+holder.time.text.toString()+" "+ holder.event.text.toString()+" "+holder.isp.text)
            alert.setCancelable(false)
            alert.setCustomTitle(alertView)
            alert.setNegativeButton( "Выйти",{dialog, i ->
                dataChanged(DataDate.getInstance(context!!).getDayEvent(dt!!,sctn!!))
                        dialog.cancel()


            })

            alert.create()
            alert.show()
            //
            var b_ne=alertView.findViewById<Button>(R.id.b_ne)
            b_ne.setOnClickListener({
                DataDate.getInstance(context!!).updateEvent(sortedList!!.get(position)!!.id,0)
               })
            var b_late=alertView.findViewById<Button>(R.id.b_late)
            b_late.setOnClickListener({
                DataDate.getInstance(context!!).updateEvent(sortedList!!.get(position)!!.id,2)
                 })
            var b_isp=alertView.findViewById<Button>(R.id.b_isp)
            b_isp.setOnClickListener({
                DataDate.getInstance(context!!).updateEvent(sortedList!!.get(position)!!.id,1)
                  })
            var b_cancel=alertView.findViewById<Button>(R.id.b_cancel)
            b_cancel.setOnClickListener({
                DataDate.getInstance(context!!).updateEvent(sortedList!!.get(position)!!.id,3)
                  })

        })
    }

    fun dataChanged(newSortedList: ArrayList<Event>) {
        list1 = newSortedList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder = EventViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.event_item, parent, false))
    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemCount() = list1!!.size //Кол-во записей
    class EventViewHolder(view: View): RecyclerView.ViewHolder(view){
        var date: TextView = view.table_date
        var time: TextView = view.table_time
        var event: TextView =view.table_event
        var isp: TextView =view.table_isp
        var btn_delete:Button=view.btn_delete
        var btn_edit:Button=view.btn_edit

    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventPageAdapter> {
        override fun createFromParcel(parcel: Parcel): EventPageAdapter {
            return EventPageAdapter()
        }

        override fun newArray(size: Int): Array<EventPageAdapter?> {
            return arrayOfNulls(size)
        }
    }
}