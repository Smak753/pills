package com.example.pills.Event

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*


class DataDate(ctx: Context) : ManagedSQLiteOpenHelper(ctx, DataDate.DB_NAME, null, DataDate.DB_VERSION) {

    var EVENT_TABLE="Timing"
    var ID="id"
    var DATE="date"
    var TIME="time"
    var EVENT="event"
    var ISP="isp"

    companion object {
        var DB_NAME = "MyDatabase1"
        var DB_VERSION = 1
        private var instance: DataDate? = null

        @Synchronized
        fun getInstance(ctx: Context): DataDate {
            if (instance == null) {
                instance = DataDate(ctx.applicationContext)
            }
            return instance!!
        }
    }

//    var dropSql:String="drop table $EVENT_TABLE"
    override fun onCreate(db: SQLiteDatabase) {
    db.dropTable(EVENT_TABLE, true)
    db.createTable(EVENT_TABLE,true,ID to INTEGER + PRIMARY_KEY,DATE to INTEGER,TIME to TEXT, EVENT to TEXT,ISP to INTEGER)
}

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
//        db.dropTable(EVENT_TABLE, true)
        if(newVersion>oldVersion)
            onCreate(db)
    }
    fun addEvent(date:Int,time:String,event:String,isp:Int): Long {
        var values = ContentValues()
        values.put(DATE,date)
        values.put(TIME,time)
        values.put(EVENT,event)
        values.put(ISP,isp)

        return this.writableDatabase.insert(EVENT_TABLE,null,values)
    }
    fun updateEvent(id:Int?,isp:Int) {
        val db = this.writableDatabase
        val query = "UPDATE $EVENT_TABLE SET $ISP = $isp WHERE $ID=$id"
        db.execSQL(query)
    }
    fun sortedEvent(id:Int?,date:Int,time:String,event:String,isp:Int):Int{
        var values = ContentValues()
        values.put(DATE,date)
        values.put(TIME,time)
        values.put(EVENT,event)
        values.put(ISP,isp)
        return this.writableDatabase.update(EVENT_TABLE,values,"$ID=?",arrayOf("$id"))
    }
    fun deleteEvent(id:Int){
        val db = this.writableDatabase
        val query = "DELETE FROM $EVENT_TABLE  WHERE $ID=$id"
        db.execSQL(query)
    }
    fun getDayEvent(today:Int,section:String):ArrayList<Event>{
        var a=""
        if(section=="after") a="$DATE>=$today"
        if(section=="before") a="$DATE<=$today"
        var list = ArrayList<Event>()
        var cursor = this.writableDatabase.query(EVENT_TABLE, arrayOf(ID,DATE,TIME,EVENT,ISP), a, null,null,null,null)
        if(cursor.moveToFirst()){
            do{
                var pill = Event()
                pill.id=cursor.getInt(0)
                pill.date=cursor.getInt(1)
                pill.time=cursor.getString(2)
                pill.event=cursor.getString(3)
                pill.isp=cursor.getInt(4)
                list.add(pill)
            }while(cursor.moveToNext())
        }
        return list

    }
}

