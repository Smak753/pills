package com.example.pills.Pills

import android.os.Parcel
import android.os.Parcelable


class Pills() :Parcelable{
    var id:Int = 0
    var name:String=""
    var img:ByteArray?=null
    var opis:String?=""
    var instr:String?=""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        name = parcel.readString()
        img = ByteArray(parcel.readInt())
//        parcel.readByteArray(img)
        opis=parcel.readString()
        instr=parcel.readString()
    }

    override   fun toString(): String {
        return "Contact(id=$id,name=$name,icon=$img,opis=$opis,instr=$instr"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeByteArray(img)
        parcel.writeString(opis)
        parcel.writeString(instr)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pills> {
        override fun createFromParcel(parcel: Parcel): Pills {
            return Pills(parcel)
        }

        override fun newArray(size: Int): Array<Pills?> {
            return arrayOfNulls(size)
        }
    }
}