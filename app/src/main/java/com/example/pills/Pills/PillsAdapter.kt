package com.example.pills.Pills


import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.pills.DbBitmapUtility
import com.example.pills.PillView
import com.example.pills.R
import kotlinx.android.synthetic.main.item.view.*

class PillsAdapter() : RecyclerView.Adapter<PillsAdapter.PillViewHolder>(), Parcelable {
    var list1:ArrayList<Pills>?=null
    var context:Context?=null


    constructor(context: Context,res:Resources):this(){
        this.list1= DataOfPills.getInstance(context!!,res).getAllPills()
        this.context=context
    }
    override fun onBindViewHolder(holder: PillViewHolder, position: Int) {
        holder.name.text = list1?.get(position)?.name
        holder.img.setImageBitmap(DbBitmapUtility.getImage(list1?.get(position)?.img))
        holder.opis.text = list1?.get(position)?.opis

        holder.img.setOnClickListener({
           var intent = Intent(holder.itemView.context, PillView::class.java)
            intent.putExtra("Pill", list1?.get(position)?.id)
           holder.itemView.context.startActivity(intent)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PillViewHolder = PillViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    override fun getItemId(position: Int): Long = position.toLong()


    override fun getItemCount() = list1!!.size //Кол-во записей

    class PillViewHolder(view: View):RecyclerView.ViewHolder(view){
        var name: TextView = view.pillName
        var img:ImageView=view.pillImage
        var opis:TextView=view.pillOpis

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PillsAdapter> {
        override fun createFromParcel(parcel: Parcel): PillsAdapter {
            return PillsAdapter()
        }

        override fun newArray(size: Int): Array<PillsAdapter?> {
            return arrayOfNulls(size)
        }
    }

}