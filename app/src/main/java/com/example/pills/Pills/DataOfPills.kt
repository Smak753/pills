package com.example.pills.Pills

import android.content.ContentValues
import android.content.Context
import android.content.res.Resources
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DataOfPills(ctx: Context,res: Resources) : ManagedSQLiteOpenHelper(ctx, "MyDatabase", null, 1) {
    var res = res
    var PILLS_TABLE="Pills"
    var ID="id"
    var NAME="name"
    var ICON="icon"
    var OPIS="opis"
    var INSTR="instr"

    companion object {
        private var instance: DataOfPills? = null

        @Synchronized
        fun getInstance(ctx: Context,res:Resources): DataOfPills {
            if (instance == null) {
                instance = DataOfPills(ctx.applicationContext,res)
            }
            return instance!!
        }
    }
    var createSql:String="create table if not exist $PILLS_TABLE($ID INTEGER PRIMARY KEY AUTOINCREMENT, $NAME TEXT, $ICON BLOB, $OPIS TEXT, $INSTR TEXT)"

    override fun onCreate(db: SQLiteDatabase) {
       // db.execSQL(createSql)
        db.createTable(PILLS_TABLE,true,ID to INTEGER + PRIMARY_KEY,NAME to TEXT,ICON to BLOB, OPIS to TEXT,INSTR to TEXT)
        db.insert(PILLS_TABLE,null,PillsDataBase(0,res))
        db.insert(PILLS_TABLE,null,PillsDataBase(1, res))
        db.insert(PILLS_TABLE,null,PillsDataBase(2, res))
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }
    fun addPills(name:String,img:ByteArray,opis:String,instr:String): Long {
        var values = ContentValues()
        values.put(NAME,name)
        values.put(ICON,img)
        values.put(OPIS,opis)
        values.put(INSTR,instr)

        return this.writableDatabase.insert(PILLS_TABLE,null,values)
    }
      fun dropTable(db: SQLiteDatabase){
          db.execSQL("DROP TABLE IF EXISTS " + PILLS_TABLE)
      }
    fun deletePills(id:Int?){
        this.writableDatabase.delete(PILLS_TABLE,"$ID=?", arrayOf("$id"))
    }

    fun getAllPills():ArrayList<Pills>{
        var list = ArrayList<Pills>()
        var cursor = this.writableDatabase.query(PILLS_TABLE, arrayOf(ID,NAME,ICON,OPIS,INSTR),null,null,null,null,null)
        if(cursor.moveToFirst()){
            do{
                var pill = Pills()
                pill.id=cursor.getInt(0)
                pill.name=cursor.getString(1)
                pill.img=cursor.getBlob(2)
                pill.opis=cursor.getString(3)
                pill.instr=cursor.getString(4)
                list.add(pill)
            }while(cursor.moveToNext())
        }
        return list

    }
    fun getPillById(id:Int?):ArrayList<Pills>{
        var list = ArrayList<Pills>()
        var cursor = this.writableDatabase.query(PILLS_TABLE, arrayOf(ID,NAME,ICON,OPIS,INSTR),"$ID==$id",null,null,null,null)
        if(cursor.moveToFirst()){
            do{
                var pill = Pills()
                pill.id=cursor.getInt(0)
                pill.name=cursor.getString(1)
                pill.img=cursor.getBlob(2)
                pill.opis=cursor.getString(3)
                pill.instr=cursor.getString(4)
                list.add(pill)
            }while(cursor.moveToNext())
        }
        return list

    }

}