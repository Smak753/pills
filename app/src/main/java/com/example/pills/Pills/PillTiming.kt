package com.example.pills.Pills

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.pills.Event.DataDate
import com.example.pills.MainActivity
import com.example.pills.Notification.DataNotif
import com.example.pills.R
import com.example.pills.TimePickerFragment
import kotlinx.android.synthetic.main.activity_pill_timing.*
import kotlinx.android.synthetic.main.activity_pill_timing.view.*
import org.jetbrains.anko.radioButton
import java.text.SimpleDateFormat
import java.util.*

class PillTiming : AppCompatActivity() {
    var dataNotif = DataNotif(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pill_timing)
        var list1: ArrayList<Pills>?
        list1 = DataOfPills.getInstance(applicationContext, resources).getPillById(intent.getIntExtra("Pills", 0))
        //
        var c = Calendar.getInstance()
        time_before_after.visibility = View.INVISIBLE
        var preferences = applicationContext.getSharedPreferences("spy", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        editor.putString("name", list1?.get(0).name)
        editor.apply()
        //

        val cal = Calendar.getInstance()
        var date = TodayArray(cal, -1)

        var pickerDialog = TimePickerFragment()
        time_1.setOnClickListener({
            pickerDialog.show(fragmentManager, list1?.get(0).name + "1")
        })
        time_2.setOnClickListener({
            pickerDialog.show(fragmentManager, list1?.get(0).name + "2")
        })
        time_3.setOnClickListener({
            pickerDialog.show(fragmentManager, list1?.get(0).name + "3")
        })
        readInfo(list1)
        //
        var before = false
        var after = false
        with_pill.isChecked=true
        radiogroup.setOnCheckedChangeListener { radiogroup, i ->
            when (i) {
                R.id.before_pill -> {
                    time_before_after.visibility = View.VISIBLE
                    before = true
                }
                R.id.after_pill -> {
                    time_before_after.visibility = View.VISIBLE
                    after = true
                }
                R.id.with_pill -> {
                    time_before_after.visibility = View.INVISIBLE
                }
            }
        }
        btn_complite.text = "Назначить курс"
        btn_complite.setOnClickListener({
            if (days.text.toString().toInt() <= 0) {
                Toast.makeText(applicationContext, "Введите дней больше 0", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (time_before_after.visibility == View.VISIBLE) {
                if (time_before_after.text.toString().toInt() >= 60 || time_before_after.text.toString().toInt() <= 0) {
                    time_before_after.hint = "--"
                    Toast.makeText(applicationContext, "Введите корректный минутный промежуток", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            }
            //
            for (i in 0..(days.text.toString().toInt() - 1)) {
                //var a=time_2.text.toString().replace(":","")
                //
                if (time_1.text.toString() != "--:--") {
                    //установка времени
                    SetEatTime(time_1, c)

                    writeDbAndMakeNotification(date[i], time_1.text.toString(), list1!!.get(0)!!.name, after, before, c)
                }
                //
                if (time_2.text.toString() != "--:--") {
                    //установка времени
                    SetEatTime(time_2, c)
                    writeDbAndMakeNotification(date[i], time_2.text.toString(), list1!!.get(0)!!.name, after, before, c)
                }
                if (time_3.text.toString() != "--:--") {
                    //установка времени
                    SetEatTime(time_3, c)
                    writeDbAndMakeNotification(date[i], time_3.text.toString(), list1!!.get(0)!!.name, after, before, c)
                }
            }
            Toast.makeText(applicationContext, "Курс лечения добавлен", Toast.LENGTH_SHORT).show()
            var intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        })


    }

    private fun writeDbAndMakeNotification(date: Int, time: String, name: String, a: Boolean, b: Boolean, c: Calendar) {
        var id: Long
        if (a) {
            addPriemPill(date, afterTime(c), "Прием пищи после " + name)
            id = dataNotif.addNotif(date, time, name)
        }
        if (b) {
            addPriemPill(date, beforeTime(c), "Прием пищи после " + name)
            id = dataNotif.addNotif(date, time, name)
        }
        addPriemPill(date, time, "Прием " + name)
        id = dataNotif.addNotif(date, time, name)
        var intent = Intent("MY_ACTION")
        intent.putExtra("KEY", "NEW_NOT")
        intent.putExtra("ID", "" + id)
//        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        sendBroadcast(intent)
    }
        override fun onResume() {
            var list1: ArrayList<Pills>?
            list1 = DataOfPills.getInstance(applicationContext, resources).getPillById(intent.getIntExtra("Pills", 0))
            readInfo(list1)
            super.onResume()
        }

        fun readInfo(list1: ArrayList<Pills>) {
            var sp = getSharedPreferences("spy", Context.MODE_PRIVATE)
            time_1.text = sp.getString(list1?.get(0).name + "1", "--:--")
            time_2.text = sp.getString(list1?.get(0).name + "2", "--:--")
            time_3.text = sp.getString(list1?.get(0).name + "3", "--:--")
        }

        fun TodayArray(cal: Calendar, prognoze: Int): ArrayList<Int> {
            var date = ArrayList<Int>()
            cal.add(Calendar.DATE, prognoze)
            for (i in 0..30) {
                cal.add(Calendar.DATE, 1)
                var format1 = SimpleDateFormat("yyyyMMdd")
                var formatted = format1.format(cal.time)
                date.add(formatted.toInt())
            }
            return date
        }

        fun SetEatTime(time: TextView, c: Calendar) {

            var timeSplit = time.text.toString().split(":")
            c.set(Calendar.HOUR_OF_DAY, timeSplit[0].toInt())
            c.set(Calendar.MINUTE, timeSplit[1].toInt())
        }

        fun afterTime(c: Calendar): String {
            c.add(Calendar.MINUTE, time_before_after.text.toString().toInt())
            var minute = c.get(Calendar.MINUTE)
            var hour = c.get(Calendar.HOUR_OF_DAY)
            return hour.toString() + ":" + minute.toString()
        }

        fun beforeTime(c: Calendar): String {
            c.add(Calendar.MINUTE, -time_before_after.text.toString().toInt())
            var minute = c.get(Calendar.MINUTE)
            var hour = c.get(Calendar.HOUR_OF_DAY)
            return hour.toString() + ":" + minute.toString()
        }

        fun addPriemPill(date: Int, time: String, event: String) {
            DataDate.getInstance(applicationContext).addEvent(date, time, event, 0)
        }
    }
