package com.example.pills.Pills

import android.content.ContentValues
import android.content.res.Resources
import android.graphics.BitmapFactory
import com.example.pills.DbBitmapUtility
import com.example.pills.R

fun PillsDataBase(i:Int,res:Resources):ContentValues{
    var NAME="name"
    var ICON="icon"
    var OPIS="opis"
    var INSTR="instr"
    var name=""
    var img:ByteArray?=null
    var opis=""
    var instr=""
    var values = ContentValues()
    when(i){
        0->{
            name="Арбидол"
                    img=DbBitmapUtility.getBytes(BitmapFactory.decodeResource(res, R.drawable.arbidol))
                    opis="Дозировка 100 мг – капсулы №1 бело-желтые – корпус белого цвета, " +
                    "крышечка желтого цвета. Содержимое капсул - смесь, содержащая " +
                    "гранулы и порошок от белого до белого с зеленовато-желтоватым " +
                    "или кремоватым оттенком цвета."
                    instr= "Лечение\n4 раза в сутки, 5 дней\nПростконтактная профилактика\n1 раз в сутки 10-14 дней"
        }
                1->{
                    name="Супрастин"
                    img=DbBitmapUtility.getBytes(BitmapFactory.decodeResource(res, R.drawable.suprastin))
                    opis="Противоаллергический препарат. Блокатор гистаминовых Н1-рецепторов.\n" +
            "Хлоропирамина гидрохлорид - хлорированный аналог трипеленамина (пирибензамина) - это классический антигистаминный препарат, принадлежащий к группе этилендиаминовых антигистаминных препаратов.\n" +
            "Оказывает антигистаминное и м-холиноблокирующее действие, обладает противорвотным эффектом, умеренной спазмолитической и периферической холиноблокирующей активностью.\n" +
            "Терапевтический эффект хлорпирамина развивается в течение 15-30 мин после приема внутрь, достигает максимума в течение первого часа после приема и продолжается минимум 3-6 ч."
                    instr=  "Взрослым назначают по 25 мг (1 таб.) 3-4 раза/сут (75-100 мг/сут).\n" +
                    "Детям в возрасте от 3 до 6 лет назначают по 1/2 таб. (12.5 мг) 2 раза/сут; детям в возрасте от 6 до 14 лет - по 1/2 таб. (12.5 мг) 2-3 раза/сут."
                }
            2->{
                name="АЦЦ"
                img=DbBitmapUtility.getBytes(BitmapFactory.decodeResource(res, R.drawable.acc))
                opis="Таблетки шипучие, 100 мг: круглые плоскоцилиндрические белого цвета, с запахом ежевики. " +
                        "Возможно наличие слабого серного запаха. Восстановленный раствор: бесцветный прозрачный с запахом ежевики. " +
                        "Возможно наличие слабого серного запаха."
                instr= "Взрослые и дети старше 14 лет: по 2 табл. шипучие 100 мг 2–3 раза в день\n" +
            "Дети от 6 до 14 лет: по 1 табл. шипучей 100 мг 3 раза в день или по 2 табл. шипучие 2 раза в день\n" +
            "Дети от 2 до 6 лет: по 1 табл. шипучей 100 мг"
            }
    }
    values.put(NAME,name)
    values.put(ICON,img)
    values.put(OPIS,opis)
    values.put(INSTR,instr)
    return values

}
