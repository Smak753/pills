package com.example.pills

import android.app.Dialog
import android.app.DialogFragment
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.TimePicker
import com.example.pills.Pills.DataOfPills
import com.example.pills.Pills.Pills
import org.jetbrains.anko.share
import java.util.*


class TimePickerFragment:DialogFragment(),TimePickerDialog.OnTimeSetListener{
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog{
        var c=Calendar.getInstance()
        var hour = c.get(Calendar.HOUR_OF_DAY)
        var minute = c.get(Calendar.MINUTE)
        return TimePickerDialog(activity,this,hour, minute,true)

    }
    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int){
        var sp= activity.getSharedPreferences("spy", Context.MODE_PRIVATE)
        var name = sp.getString("name","--:--")
       var id = when(tag){
           name+"1"-> R.id.time_1
           name+"2"-> R.id.time_2
           name+"3"-> R.id.time_3
           else->2
        }
        var myHour = hourOfDay
        var hour=myHour.toString()
        if (myHour<10)hour="0"+myHour
        var myMinute = minute
        var minute=myMinute.toString()
        if (myMinute<10)minute="0"+myMinute
        var time=""+hour+":"+minute
        var preferences=activity.getSharedPreferences("spy", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        editor.putString(tag,time)
        editor.apply()
        var tv = activity.findViewById(id) as TextView
        tv.text=time
    }

    override fun onCancel(dialog: DialogInterface?) {
        var sp= activity.getSharedPreferences("spy", Context.MODE_PRIVATE)
        var name = sp.getString("name","--:--")
        var id = when(tag) {
            name + "1" -> R.id.time_1
            name + "2" -> R.id.time_2
            name + "3" -> R.id.time_3
            else -> 2
        }
        var tv = activity.findViewById(id) as TextView
        tv.text="--:--"
        var preferences=activity.getSharedPreferences("spy", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        editor.putString(tag,tv.text.toString())
        editor.apply()
    }
}