package com.example.pills.Notification

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DataNotif(ctx: Context) : ManagedSQLiteOpenHelper(ctx, DataNotif.DB_NAME, null, DataNotif.DB_VERSION) {

    var NOTIF_TABLE="Notification"
    var ID="id"
    var DATE="date"
    var TIME="time"
    var NAME="name"

    companion object {
        var DB_NAME = "MyDatabase2"
        var DB_VERSION = 1
        private var instance: DataNotif? = null

        @Synchronized
        fun getInstance(ctx: Context): DataNotif {
            if (instance == null) {
                instance = DataNotif(ctx.applicationContext)
            }
            return instance!!
        }
    }
    override fun onCreate(db: SQLiteDatabase) {
        db.dropTable(NOTIF_TABLE, true)
        db.createTable(NOTIF_TABLE,true,ID to INTEGER + PRIMARY_KEY,DATE to INTEGER,TIME to TEXT, NAME to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if(newVersion>oldVersion)
            onCreate(db)
    }
    fun addNotif(date:Int,time:String,name:String): Long {
        var values = ContentValues()
        values.put(DATE,date)
        values.put(TIME,time)
        values.put(NAME,name)

        return this.writableDatabase.insert(NOTIF_TABLE,null,values)
    }
    fun updateNotif(id:Int?,time:String){
        val db = this.writableDatabase
        val query = "UPDATE $NOTIF_TABLE SET $TIME = $time WHERE $ID = $id"
        db.execSQL(query)

    }
    fun deleteNotif(id:Int?){
        this.writableDatabase.delete(NOTIF_TABLE,"$ID=?", arrayOf("$id"))
    }
    fun getNotif(id:Int?):ArrayList<Notif>{

        var list = ArrayList<Notif>()
        var cursor = this.writableDatabase.query(NOTIF_TABLE, arrayOf(ID,DATE,TIME,NAME), "$ID=$id", null,null,null,null)
        if(cursor.moveToFirst()){
            do{
                var notif = Notif()
                notif.id=cursor.getInt(0)
                notif.date=cursor.getInt(1)
                notif.time=cursor.getString(2)
                notif.name=cursor.getString(3)
                list.add(notif)
            }while(cursor.moveToNext())
        }
        return list

    }
}