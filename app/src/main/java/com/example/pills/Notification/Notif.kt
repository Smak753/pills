package com.example.pills.Notification

import android.os.Parcel
import android.os.Parcelable

class Notif() : Parcelable {
    var id:Int = 0
    var date:Int=0
    var time:String= ""
    var name:String=""


    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        date=parcel.readInt()
        time=parcel.readString()
        name=parcel.readString()
    }

    override   fun toString(): String {
        return "Contact(id=$id,date=$date,time=$time,name=$name"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(date)
        parcel.writeString(time)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Notif> {
        override fun createFromParcel(parcel: Parcel): Notif {
            return Notif(parcel)
        }

        override fun newArray(size: Int): Array<Notif?> {
            return arrayOfNulls(size)
        }
    }
}