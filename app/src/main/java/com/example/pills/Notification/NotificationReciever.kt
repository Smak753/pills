package com.example.pills.Notification

import android.app.AlarmManager
import android.app.Notification
import android.content.Context
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Intent
import android.preference.PreferenceManager
import com.example.pills.Event.DataDate
import com.example.pills.R
import java.util.*
import kotlin.collections.ArrayList

class NotificationReciever : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        var sp = PreferenceManager.getDefaultSharedPreferences(context)
        var late = sp.getString("lateTime", "30").toInt()*60
        var receivedValue = intent.getStringExtra("KEY")
        var dataNotif:ArrayList<Notif>
        fun scheduleNotification(notif: Notification, hour: Int, min: Int, id: Int) {
            val intent = Intent(context, NotifReciev::class.java)
            intent.putExtra("ID", id)
            intent.putExtra(NotifReciev.NOTIFICATION, notif)
            val pendingIntent = PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val calendar = Calendar.getInstance()
            val setCalendar = Calendar.getInstance()
            setCalendar.set(Calendar.HOUR_OF_DAY, hour)
            setCalendar.set(Calendar.MINUTE, min)
            setCalendar.set(Calendar.SECOND, 0)
            intent.putExtra("MINUTE", min)
            intent.putExtra("HOUR", hour)
            intent.putExtra("collapse", late)
            if (setCalendar.before(calendar))
                setCalendar.add(Calendar.DATE, 1)
            var time = setCalendar.timeInMillis
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent)
        }
        fun getNotification(id:Int, name: String): Notification {
            var text=  name
            var key1 = "CANCEL"
            var key2 = "ACCEPT"
            var key3 = "LATE"

            var intentCancel = Intent("MY_ACTION")
            intentCancel.putExtra("KEY", key1)
            intentCancel.putExtra("ID", id)
            var cancelPendingIntent = PendingIntent.getBroadcast(context, 0, intentCancel, PendingIntent.FLAG_UPDATE_CURRENT)

            var intentAccept = Intent("MY_ACTION")
            intentAccept.putExtra("KEY", key2)
            intentAccept.putExtra("ID", id)
            var acceptPendingIntent = PendingIntent.getBroadcast(context, 1, intentAccept, PendingIntent.FLAG_UPDATE_CURRENT)

            var intentPostpone  = Intent("MY_ACTION")
            intentPostpone.putExtra("KEY", key3)
            intentPostpone.putExtra("ID", id)
            var postponePendingIntent = PendingIntent.getBroadcast(context, 2, intentPostpone, PendingIntent.FLAG_UPDATE_CURRENT)

            val builder = Notification.Builder(context)
            builder.setContentTitle(R.string.app_name.toString())
            builder.setContentText(text)
            builder.setSmallIcon(R.drawable.ic_pill)
            builder.addAction(R.drawable.ic_cancel, "Отмена", cancelPendingIntent)
            builder.addAction(R.drawable.ic_late, "Отложить", postponePendingIntent)
            builder.addAction(R.drawable.ic_done, "Принять", acceptPendingIntent)

            return builder.build()
        }

        when (receivedValue) {
            "CANCEL" -> {
                var id = intent.getIntExtra("ID", 0)
                dataNotif= DataNotif(context).getNotif(id)
                DataDate(context).updateEvent(id,3)
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancel(id)
                //Toast.makeText(context, "CANCEL id: $id", Toast.LENGTH_SHORT).show()
            }
            "LATE" -> {
                var id = intent.getIntExtra("ID", 0)
                dataNotif= DataNotif(context).getNotif(id)
                var name = dataNotif!![0].name
                var c= Calendar.getInstance()
                c.set(Calendar.MINUTE,late)
                var min=c.get(Calendar.MINUTE)
                var hour = c.get(Calendar.HOUR_OF_DAY)
                DataDate(context).updateEvent(id,3)
                scheduleNotification(getNotification(id,  name),  hour, min,id)
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancel(id)
            }
            "ACCEPT" -> {
                var id = intent.getIntExtra("ID", 0)
                dataNotif= DataNotif(context).getNotif(id)
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                DataDate(context).updateEvent(id,1)
                notificationManager.cancel(id)
                //Toast.makeText(context, "ACCEPT id: $id", Toast.LENGTH_SHORT).show()
            }
            "NEW_NOT"->{
                var id = intent.getStringExtra("ID")
                dataNotif= DataNotif(context).getNotif(id.toInt())
                 var hourMin=dataNotif!![0].time.replace(":","")
                var hour=hourMin[0]+""+hourMin[1]
                if(hourMin[0].toString()=="0"){hour=""+hourMin[1]}
                var min=hourMin[2]+""+hourMin[3]
                if(hourMin[2].toString()=="0"){min=""+hourMin[3]}
                var name = dataNotif!![0].name
                    scheduleNotification(getNotification(id.toInt(), name),  hour.toInt(), min.toInt(),id.toInt())

            }

    }
}}