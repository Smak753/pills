package com.example.pills

import android.content.ContentProvider
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.pills.Event.DataDate
import kotlinx.android.synthetic.main.activity_main.*
import com.example.pills.Event.EventPageAdapter
import com.example.pills.Pills.PillsAdapter
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
   private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.pills -> {

            list1.visibility = View.VISIBLE
                linearLayout.visibility=View.INVISIBLE
            event_list.visibility = View.INVISIBLE
//                scheduleStop()
                return@OnNavigationItemSelectedListener true
            }
            R.id.timing -> {

                list1.visibility = View.INVISIBLE
                linearLayout.visibility=View.VISIBLE
                event_list.visibility = View.VISIBLE
                fillAdapterAfter()
                return@OnNavigationItemSelectedListener true
            }
            R.id.history -> {

                list1.visibility = View.INVISIBLE
                linearLayout.visibility=View.VISIBLE
                event_list.visibility = View.VISIBLE
                fillAdapterBefore()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    private fun fillAdapterAfter() {
        var c=Calendar.getInstance()
        formatedDate(c)
        event_list.layoutManager =LinearLayoutManager(applicationContext)
        event_list.adapter = EventPageAdapter(this@MainActivity,formatedDate(c),"after",this)
    }
    private fun fillAdapterBefore() {
        var c=Calendar.getInstance()
        formatedDate(c)
        event_list.layoutManager =LinearLayoutManager(applicationContext)
        event_list.adapter = EventPageAdapter(this@MainActivity,formatedDate(c),"before",this)
    }
    fun formatedDate(c:Calendar):Int{
        var format1 = SimpleDateFormat("yyyyMMdd")
        var formatted = format1.format(c.time)
        var i= formatted.toInt()
        return i
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        list1.visibility = View.VISIBLE
        linearLayout.visibility=View.INVISIBLE
        event_list.visibility=View.INVISIBLE
        //
        list1.layoutManager = LinearLayoutManager(applicationContext)
        list1.adapter = PillsAdapter(this@MainActivity,resources)

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
         when (item.itemId) {
             R.id.settings -> {
                 var intent = Intent(applicationContext, PrefActivity::class.java)
                 startActivity(intent)
             }
         }
            return super.onOptionsItemSelected(item)
    }

}
