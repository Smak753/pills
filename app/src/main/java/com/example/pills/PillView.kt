package com.example.pills


import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.pills.Event.DataDate
import com.example.pills.Pills.DataOfPills
import com.example.pills.Pills.PillTiming
import com.example.pills.Pills.Pills
import kotlinx.android.synthetic.main.activity_pill_view.*
import java.text.SimpleDateFormat
import java.util.*

class PillView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pill_view)
        var list1: ArrayList<Pills>?
        list1= DataOfPills.getInstance(applicationContext,resources).getPillById(intent.getIntExtra("Pill",0))
        img.setImageBitmap(DbBitmapUtility.getImage(list1?.get(0).img))
        name.text=list1?.get(0).name
        opis.text=list1?.get(0).opis
        instr.text=list1?.get(0).instr
//        var i=date[0].replace("-","")
//        var b=i.toInt()
//        DataDate.getInstance(applicationContext).addEvent(
//                "2018-01-25",
//                "",
//                "Прием "+list1?.get(0).name,
//                0)
        btn_instr.text="Назначить прием"
       // var a=time_1.text.toString().replace(":","")
        btn_instr.setOnClickListener({
            var intent = Intent(applicationContext, PillTiming::class.java)
            intent.putExtra("Pills", list1?.get(0).id)
            startActivity(intent)


//
        })


    }
}
